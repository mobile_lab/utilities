//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>

#include <geometry_msgs/TwistStamped.h>

class RepeatTopic
{
  public:
  RepeatTopic();
  ~RepeatTopic();
private:
  ros::NodeHandle n;
  ros::Subscriber twist_sub;
  ros::Publisher repeat_twist_pub;
  geometry_msgs::TwistStamped twist_;

  void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);

  ros::Timer timer_;
  void timer_callback(const ros::TimerEvent& e);

  

};
RepeatTopic::RepeatTopic()
{
  int rate = 10;
  repeat_twist_pub = n.advertise<geometry_msgs::TwistStamped>("/twist_cmd", 1, true);
  twist_sub = n.subscribe("interface_twist_cmd", 1000, &RepeatTopic::twistCallback, this);
  timer_ = n.createTimer(ros::Duration(1.0 / rate), &RepeatTopic::timer_callback, this);
  ros::spin();
}
RepeatTopic::~RepeatTopic(){}

void RepeatTopic::timer_callback(const ros::TimerEvent& e)
{
  repeat_twist_pub.publish(twist_);
}

void RepeatTopic::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{

  twist_ = *msg;

}






int main (int argc, char *argv[])

{
  ros::init(argc, argv, "cmd_repeater");
  RepeatTopic RT;
  
  return 0;
}





