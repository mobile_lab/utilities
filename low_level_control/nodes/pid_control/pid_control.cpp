//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
// #include "autoware_msgs/SteerCmd.h"
// #include "autoware_msgs/AccelCmd.h"
#include <std_msgs/Float64.h>
#include "autoware_msgs/VehicleCmd.h"
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/AccelStamped.h>
//#include <math.h>

#include <dynamic_reconfigure/server.h>
#include <low_level_control/PIDConfig.h>

#include <fstream>

//tmp define outside of the class just for dynamic reconfigure
const static int num_of_gains_ = 4;
  double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  double gain_steps_[num_of_gains_];
  double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;
  bool enable_log_to_file_ = false, first_flag_ = false; 
double vel_desired_;
double sigmoid_k = 10;
bool overide_vel_ = false;
std::string filename_;
class PIDcontrol
{
  public:
  PIDcontrol();
  ~PIDcontrol();
private:
  ros::NodeHandle n;
  ros::Subscriber vehicle_cmd_sub,twist_sub,accel_sub;
  ros::Publisher accel_pub,steer_pub;

  std_msgs::Float64 steer;
  std_msgs::Float64 accel;
  void cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg);
  void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
  void accelCallback(const geometry_msgs::AccelStamped::ConstPtr& msg);

  int velToindex(double vel);
  void updateParams();

void log_to_file(double t,bool first_flag, std::string filename, int vel_ind,double error_dot,double d_accel_cmd,double dt) const;

  // const static int num_of_gains_ = 4;
  // double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  // double gain_steps_[num_of_gains_];
  // double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;
  //double vel_desired_;
  double  vel_,error_, prev_error_ ,prev_vel_ = 0,error_int_ = 0;
  double steer_des_,accel_cmd_,prev_accel_cmd_ = 0;
  double prev_t_ = ros::Time::now().toSec();
double t_;
double accel_ = 0;

double kp_e;







};
PIDcontrol::PIDcontrol()
{
  steer_des_ = 0;
  vel_desired_ = 0;

  // gain_steps_[0] = 0;
  // gain_steps_[1] = 2;
  // gain_steps_[2] = 4;
  // gain_steps_[3] = 7;


  // kp_[0] = 1;
  // kd_[0] = 0;
  // ki_[0] = 0;

  // kp_[1] = 1;
  // kd_[1] = 0;
  // ki_[1] = 0;

  // kp_[2] = 1;
  // kd_[2] = 0;
  // ki_[2] = 0;

  // kp_[3] = 1;
  // kd_[3] = 0;
  // ki_[3] = 0;
  updateParams();
  vehicle_cmd_sub = n.subscribe("/vehicle_cmd", 1000, &PIDcontrol::cmdCallback, this);
  twist_sub = n.subscribe("current_velocity", 1000, &PIDcontrol::twistCallback, this);
  accel_sub = n.subscribe("gnss_acceleration", 1000, &PIDcontrol::accelCallback, this);
  steer_pub = n.advertise<std_msgs::Float64>("steer_cmd", 10);
  accel_pub = n.advertise<std_msgs::Float64>("accel_cmd", 10);





  ros::spin();
}
PIDcontrol::~PIDcontrol(){}



void PIDcontrol::updateParams()
{
  n.param("gain_steps1", gain_steps_[0], 0.0);
  n.param("gain_steps2", gain_steps_[1], 2.0);
  n.param("gain_steps3", gain_steps_[2], 4.0);
  n.param("gain_steps4", gain_steps_[3], 7.0);

  n.param("kp1", kp_[0], 0.01);
  n.param("kd1", kd_[0], 0.015);
  n.param("ki1", ki_[0], 0.0);

  n.param("kp2", kp_[1], 0.01);
  n.param("kd2", kd_[1], 0.015);
  n.param("ki2", ki_[1], 0.0);

  n.param("kp3", kp_[2], 0.01);
  n.param("kd3", kd_[2], 0.005);
  n.param("ki3", ki_[2], 0.0);

  n.param("kp4", kp_[3], 0.01);
  n.param("kd4", kd_[3], 0.008);
  n.param("ki4", ki_[3], 0.0);

  n.param("max_throttle_acc", max_throttle_acc_, 1.0);
  n.param("min_throttle_acc", min_throttle_acc_, -1.0);
  n.param("max_brake_acc", max_brake_acc_, 1.0);
  n.param("min_brake_acc", min_brake_acc_, -1.0);

  n.param("max_throttle", max_throttle_, 0.4);
  n.param("min_throttle", min_throttle_, -0.4);


  for (int i = 0; i < num_of_gains_;i++)
  {
    ROS_WARN_STREAM("gain_steps"<<i<<": "<<gain_steps_[i]<<" kp: "<<kp_[i]<< " kd: "<< kd_[i]<< " ki: "<<ki_[i]);
  }

  ROS_WARN_STREAM("max_throttle_acc: "<<max_throttle_acc_<<" min_throttle_acc: "<<min_throttle_acc_<<" max_brake_acc: "<<max_brake_acc_<<" min_brake_acc: "<<min_brake_acc_);

}

void PIDcontrol::log_to_file(double t1,bool first_flag,std::string filename,int vel_ind,double error_dot,double d_accel_cmd,double dt) const
{
  
  std::ofstream ofs(filename, std::ios::app);//+std::to_string(ros::Time::now().toSec)
  // first subscribe
  if (first_flag)
  {
    ofs << "t,gain_steps,vel,vel_desired_,error_,error_dot,accel,d_accel_cmd,accel_cmd_,dt, kp,kd " << std::endl;
  }
  else
  {
    ofs << std::fixed << std::setprecision(6) <<t1<<","<< vel_ind << "," << vel_ << ","
    << vel_desired_ << "," << error_ <<","<< error_dot<<","<<accel_<< "," << d_accel_cmd <<","<< accel_cmd_ << ","<<dt<<","<<kp_[0]<<","<<kd_[0]<<  std::endl;
    
  }
}

int PIDcontrol::velToindex(double vel)
{
  for (int i =1;i<num_of_gains_;i++)
  {
    if(vel < gain_steps_[i] )
      return i-1;
  }
  return num_of_gains_-1;
}

void PIDcontrol::accelCallback(const geometry_msgs::AccelStamped::ConstPtr& msg)
{
  accel_ = msg->accel.linear.x;
}

void PIDcontrol::cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg)
{
  //ROS_WARN_STREAM("cmd callback");
  if (!overide_vel_)
    vel_desired_ = msg->twist_cmd.twist.linear.x;///////////////////////////////
steer_des_ = msg->twist_cmd.twist.angular.z;
}

void PIDcontrol::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  //updateParams();

  vel_ = msg->twist.linear.x;
  error_ = vel_desired_ - vel_;
  double t = msg->header.stamp.toSec();
  double dt = t - prev_t_;
  double error_dot = (error_-prev_error_)/dt;//accel_;//
  error_int_ += error_*dt;

  int vel_ind = velToindex(vel_);

   double d_accel_cmd = kp_[vel_ind]* error_+ kd_[vel_ind]* error_dot + ki_[vel_ind]* error_int_;
   //kp_e =  (1./(1.+exp(-abs( error_)*sigmoid_k)))*kp_[vel_ind];
   //double d_accel_cmd = kp_e* error_+ kd_[vel_ind]* error_dot + ki_[vel_ind]* error_int_;
  //ROS_WARN_STREAM("gain index: "<<vel_ind<<" kp: "<< kp_[vel_ind]<<" kd: "<<kd_[vel_ind]<<" ki: "<<ki_[vel_ind]);


//throttle_area and accelerate - max_throttle_acc
//throttle_area and deccelerate
//brake_area and accelerate
//brake_area and deccelerate

  if (prev_accel_cmd_ > 0)//throttle 
  {
      d_accel_cmd =  std::min(max_throttle_acc_/dt,d_accel_cmd);
      d_accel_cmd =  std::max(min_throttle_acc_/dt,d_accel_cmd);
  }
  else//brake 
  {
      d_accel_cmd =  std::min(max_brake_acc_/dt,d_accel_cmd);
      d_accel_cmd =  std::max(min_brake_acc_/dt,d_accel_cmd);
  } 


  accel_cmd_ = prev_accel_cmd_ + d_accel_cmd;

  accel_cmd_ = std::min(accel_cmd_,max_throttle_);
  accel_cmd_ = std::max(accel_cmd_,min_throttle_);

  //steer_des_ = 0;//tmp for pid tunning
  if(vel_ < 0.3 && vel_desired_ < 0.01)
    accel_cmd_ = -0.25;//min_throttle_;
  accel.data = accel_cmd_;
  steer.data = steer_des_;

//ROS_WARN_STREAM(" vel_: "<<vel_<<" vel_desired_: "<< vel_desired_);//<<" dt: "<<dt

if (enable_log_to_file_)
{
  if (first_flag_)
  {
  filename_ = "log_pid_"+std::to_string(ros::Time::now().toSec());
      t_ = 0;
  log_to_file(t_,true,filename_, vel_ind,error_dot,d_accel_cmd,dt);
  first_flag_ = false;
  }
  else
  t_+=dt;
  log_to_file(t_,false,filename_, vel_ind,error_dot,d_accel_cmd,dt);

}


  prev_t_ = t;
  prev_error_ = error_;
  prev_accel_cmd_ = accel_cmd_;


  

  
  steer_pub.publish(steer);
  accel_pub.publish(accel);
}

  // double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  // double gain_steps_[num_of_gains_];
  // double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;

void callback(low_level_control::PIDConfig &config, uint32_t level) {
  // ROS_INFO("Reconfigure Request: %d %f %s %s %d", 
  //           config.int_param, config.double_param, 
  //           config.str_param.c_str(), 
  //           config.bool_param?"True":"False", 
  //           config.size);
  if (!enable_log_to_file_ &&config.enable_log)
  {
    first_flag_ = true;

  }
enable_log_to_file_ = config.enable_log;
overide_vel_ = config.overide_vel;
if (overide_vel_)
  vel_desired_ = config.vel_desired;
//sigmoid_k = config.sigmoid_k;
  gain_steps_[0] = config.gain_steps1;
  gain_steps_[1] = config.gain_steps2;
  gain_steps_[2] = config.gain_steps3;
  gain_steps_[3] = config.gain_steps4;


  kp_[0] = config.kp1;
  kd_[0] = config.kd1;
  ki_[0] = config.ki1;

  kp_[1] = config.kp2;
  kd_[1] = config.kd2;
  ki_[1] = config.ki2;

  kp_[2] = config.kp3;
  kd_[2] = config.kd3;
  ki_[2] = config.ki3;

  kp_[3] = config.kp4;
  kd_[3] = config.kd4;
  ki_[3] = config.ki4;

  max_throttle_acc_ = config.max_brake_acc;
  min_throttle_acc_ = config.min_brake_acc;
  max_brake_acc_ = config.max_brake_acc;
  min_brake_acc_ = config.min_brake_acc;

  max_throttle_ = config.max_throttle;
  min_throttle_ = config.min_throttle;


  for (int i = 0; i < num_of_gains_;i++)
  {
    ROS_WARN_STREAM("gain_steps"<<i<<": "<<gain_steps_[i]<<" kp: "<<kp_[i]<< " kd: "<< kd_[i]<< " ki: "<<ki_[i]);
  }

  ROS_WARN_STREAM("max_throttle_acc: "<<max_throttle_acc_<<" min_throttle_acc: "<<min_throttle_acc_<<" max_brake_acc: "<<max_brake_acc_<<" min_brake_acc: "<<min_brake_acc_);

 }


int main (int argc, char *argv[])

{
  ros::init(argc, argv, "pid_control");
  dynamic_reconfigure::Server<low_level_control::PIDConfig> server;
  dynamic_reconfigure::Server<low_level_control::PIDConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  server.setCallback(f);

  
  PIDcontrol pidC;
  
  return 0;
}





