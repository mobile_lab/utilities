//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
// #include "autoware_msgs/SteerCmd.h"
// #include "autoware_msgs/AccelCmd.h"
#include <std_msgs/Float64.h>
#include "autoware_msgs/VehicleCmd.h"
#include <geometry_msgs/TwistStamped.h>
//#include <math.h>

#include <dynamic_reconfigure/server.h>
#include <low_level_control/PIDConfig.h>
//tmp define outside of the class just for dynamic reconfigure
const static int num_of_gains_ = 4;
  double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  double gain_steps_[num_of_gains_];
  double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;
double vel_desired_;
class PIDcontrol
{
  public:
  PIDcontrol();
  ~PIDcontrol();
private:
  ros::NodeHandle n;
  ros::Subscriber vehicle_cmd_sub,twist_sub;
  ros::Publisher accel_pub,steer_pub;

  std_msgs::Float64 steer;
  std_msgs::Float64 accel;
  void cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg);
  void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);

  int velToindex(double vel);
  void updateParams();


  // const static int num_of_gains_ = 4;
  // double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  // double gain_steps_[num_of_gains_];
  // double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;
  //double vel_desired_;
  double  vel_,error_, prev_error_ ,prev_vel_ = 0,error_int_ = 0;
  double steer_des_,accel_cmd_,prev_accel_cmd_ = 0;
  double prev_t_ = ros::Time::now().toSec();






};
PIDcontrol::PIDcontrol()
{
  steer_des_ = 0;
  vel_desired_ = 0;

  // gain_steps_[0] = 0;
  // gain_steps_[1] = 2;
  // gain_steps_[2] = 4;
  // gain_steps_[3] = 7;


  // kp_[0] = 1;
  // kd_[0] = 0;
  // ki_[0] = 0;

  // kp_[1] = 1;
  // kd_[1] = 0;
  // ki_[1] = 0;

  // kp_[2] = 1;
  // kd_[2] = 0;
  // ki_[2] = 0;

  // kp_[3] = 1;
  // kd_[3] = 0;
  // ki_[3] = 0;
  updateParams();
  vehicle_cmd_sub = n.subscribe("/vehicle_cmd", 1000, &PIDcontrol::cmdCallback, this);
  twist_sub = n.subscribe("current_velocity", 1000, &PIDcontrol::twistCallback, this);

  steer_pub = n.advertise<std_msgs::Float64>("steer_cmd", 10);
  accel_pub = n.advertise<std_msgs::Float64>("accel_cmd", 10);





  ros::spin();
}
PIDcontrol::~PIDcontrol(){}



void PIDcontrol::updateParams()
{
  n.param("gain_steps1", gain_steps_[0], 0.0);
  n.param("gain_steps2", gain_steps_[1], 2.0);
  n.param("gain_steps3", gain_steps_[2], 4.0);
  n.param("gain_steps4", gain_steps_[3], 7.0);

  n.param("kp1", kp_[0], 1.0);
  n.param("kd1", kd_[0], 0.0);
  n.param("ki1", ki_[0], 0.0);

  n.param("kp2", kp_[1], 1.0);
  n.param("kd2", kd_[1], 0.0);
  n.param("ki2", ki_[1], 0.0);

  n.param("kp3", kp_[2], 1.0);
  n.param("kd3", kd_[2], 0.0);
  n.param("ki3", ki_[2], 0.0);

  n.param("kp4", kp_[3], 1.0);
  n.param("kd4", kd_[3], 0.0);
  n.param("ki4", ki_[3], 0.0);

  n.param("max_throttle_acc", max_throttle_acc_, 1.0);
  n.param("min_throttle_acc", min_throttle_acc_, -1.0);
  n.param("max_brake_acc", max_brake_acc_, 1.0);
  n.param("min_brake_acc", min_brake_acc_, -1.0);

  n.param("max_throttle", max_throttle_, 0.3);
  n.param("min_throttle", min_throttle_, -0.3);


  for (int i = 0; i < num_of_gains_;i++)
  {
    ROS_WARN_STREAM("gain_steps"<<i<<": "<<gain_steps_[i]<<" kp: "<<kp_[i]<< " kd: "<< kd_[i]<< " ki: "<<ki_[i]);
  }

  ROS_WARN_STREAM("max_throttle_acc: "<<max_throttle_acc_<<" min_throttle_acc: "<<min_throttle_acc_<<" max_brake_acc: "<<max_brake_acc_<<" min_brake_acc: "<<min_brake_acc_);

}
int PIDcontrol::velToindex(double vel)
{
  for (int i =1;i<num_of_gains_;i++)
  {
    if(vel < gain_steps_[i] )
      return i-1;
  }
  return num_of_gains_-1;
}

void PIDcontrol::cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg)
{
  ROS_WARN_STREAM("cmd callback");
//vel_desired_ = msg->twist_cmd.twist.linear.x;///////////////////////////////
steer_des_ = msg->twist_cmd.twist.angular.z;
}

void PIDcontrol::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  //updateParams();
  vel_ = msg->twist.linear.x;
  error_ = vel_desired_ - vel_;
  double t = msg->header.stamp.toSec();
  double dt = t - prev_t_;
  double error_dot = (error_-prev_error_)/dt;
  error_int_ += error_*dt;

  int vel_ind = velToindex(vel_);
   double d_accel_cmd = kp_[vel_ind]* error_+ kd_[vel_ind]* error_dot + ki_[vel_ind]* error_int_;
  //ROS_WARN_STREAM("gain index: "<<vel_ind<<" kp: "<< kp_[vel_ind]<<" kd: "<<kd_[vel_ind]<<" ki: "<<ki_[vel_ind]);


//throttle_area and accelerate - max_throttle_acc
//throttle_area and deccelerate
//brake_area and accelerate
//brake_area and deccelerate

  if (prev_accel_cmd_ > 0)//throttle 
  {
      d_accel_cmd =  std::min(max_throttle_acc_/dt,d_accel_cmd);
      d_accel_cmd =  std::max(min_throttle_acc_/dt,d_accel_cmd);
  }
  else//brake 
  {
      d_accel_cmd =  std::min(max_brake_acc_/dt,d_accel_cmd);
      d_accel_cmd =  std::max(min_brake_acc_/dt,d_accel_cmd);
  } 


  accel_cmd_ = prev_accel_cmd_ + d_accel_cmd;

  accel_cmd_ = std::min(accel_cmd_,max_throttle_);
  accel_cmd_ = std::max(accel_cmd_,min_throttle_);

  //steer_des_ = 0;//tmp for pid tunning
  if(vel_ < 0.5 && vel_desired_ < 0.01)
    accel_cmd_ = min_throttle_;
  accel.header.stamp = ros::Time::now();
  accel.data = accel_cmd_;
  steer.data = steer_des_;

ROS_WARN_STREAM("vel_: "<<vel_<<" vel_desired_: "<< vel_desired_<<" accel: "<<accel.data<<" error_: "<<error_<<" d_accel_cmd: "<<d_accel_cmd<<" dt: "<<dt);//<<" dt: "<<dt

  prev_t_ = t;
  prev_error_ = error_;
  prev_accel_cmd_ = accel_cmd_;


  

  
  steer_pub.publish(steer);
  accel_pub.publish(accel);
}

  // double kp_[num_of_gains_],kd_[num_of_gains_],ki_[num_of_gains_];
  // double gain_steps_[num_of_gains_];
  // double max_throttle_acc_, min_throttle_acc_, max_brake_acc_, min_brake_acc_, max_throttle_,min_throttle_;

void callback(low_level_control::PIDConfig &config, uint32_t level) {
  // ROS_INFO("Reconfigure Request: %d %f %s %s %d", 
  //           config.int_param, config.double_param, 
  //           config.str_param.c_str(), 
  //           config.bool_param?"True":"False", 
  //           config.size);

vel_desired_ = config.vel_desired;
  gain_steps_[0] = config.gain_steps1;
  gain_steps_[1] = config.gain_steps2;
  gain_steps_[2] = config.gain_steps3;
  gain_steps_[3] = config.gain_steps4;


  kp_[0] = config.kp1;
  kd_[0] = config.kd1;
  ki_[0] = config.ki1;

  kp_[1] = config.kp2;
  kd_[1] = config.kd2;
  ki_[1] = config.ki2;

  kp_[2] = config.kp3;
  kd_[2] = config.kd3;
  ki_[2] = config.ki3;

  kp_[3] = config.kp4;
  kd_[3] = config.kd4;
  ki_[3] = config.ki4;

  max_throttle_acc_ = config.max_brake_acc;
  min_throttle_acc_ = config.min_brake_acc;
  max_brake_acc_ = config.max_brake_acc;
  min_brake_acc_ = config.min_brake_acc;

  max_throttle_ = config.max_throttle;
  min_throttle_ = config.min_throttle;

  for (int i = 0; i < num_of_gains_;i++)
  {
    ROS_WARN_STREAM("gain_steps"<<i<<": "<<gain_steps_[i]<<" kp: "<<kp_[i]<< " kd: "<< kd_[i]<< " ki: "<<ki_[i]);
  }

  ROS_WARN_STREAM("max_throttle_acc: "<<max_throttle_acc_<<" min_throttle_acc: "<<min_throttle_acc_<<" max_brake_acc: "<<max_brake_acc_<<" min_brake_acc: "<<min_brake_acc_);

 }


int main (int argc, char *argv[])

{
  ros::init(argc, argv, "pid_control");
  dynamic_reconfigure::Server<low_level_control::PIDConfig> server;
  dynamic_reconfigure::Server<low_level_control::PIDConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  server.setCallback(f);

  
  PIDcontrol pidC;
  
  return 0;
}





