//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
// #include "autoware_msgs/SteerCmd.h"
// #include "autoware_msgs/AccelCmd.h"
#include <std_msgs/Float64.h>
#include "autoware_msgs/VehicleCmd.h"

class DirectCommand
{
  public:
  DirectCommand();
  ~DirectCommand();
private:
  ros::NodeHandle n;
  ros::Subscriber vehicle_cmd_sub;
  ros::Publisher accel_pub,steer_pub;

  std_msgs::Float64 steer;
  std_msgs::Float64 accel;
  void cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg);

};
DirectCommand::DirectCommand()
{
  

  vehicle_cmd_sub = n.subscribe("vehicle_cmd", 1000, &DirectCommand::cmdCallback, this);

  steer_pub = n.advertise<std_msgs::Float64>("steer_cmd", 10);
  accel_pub = n.advertise<std_msgs::Float64>("accel_cmd", 10);
  ros::spin();
}
DirectCommand::~DirectCommand(){}


void DirectCommand::cmdCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg)
{
double vel_desired_ = msg->twist_cmd.twist.linear.x/50-1;
double steer_des_ = msg->twist_cmd.twist.angular.z;


ROS_WARN_STREAM( "vel_desired_: "<< vel_desired_<<" acc: "<<accel.data);
  steer_pub.publish(steer_des_);
  accel_pub.publish(vel_desired_);
}








int main (int argc, char *argv[])

{
  ros::init(argc, argv, "direct_command");
  DirectCommand pidC;
  
  return 0;
}





